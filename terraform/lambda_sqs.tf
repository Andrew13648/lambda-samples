resource "aws_lambda_function" "sqs_lambda" {
  filename         = "../bin/sqs-lambda.zip"
  function_name    = "sqs-lambda"
  role             = aws_iam_role.sqs_role.arn
  handler          = "sqs_lambda.lambda_handler"
  source_code_hash = filebase64sha256("../bin/sqs-lambda.zip")
  runtime          = "python3.8"
}

resource "aws_lambda_event_source_mapping" "sqs_event" {
  event_source_arn = aws_sqs_queue.queue.arn
  function_name    = aws_lambda_function.sqs_lambda.arn
}
