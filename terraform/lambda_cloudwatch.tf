resource "aws_lambda_function" "cloudwatch_lambda" {
  filename         = "../bin/cloudwatch-lambda.zip"
  function_name    = "cloudwatch-lambda"
  role             = aws_iam_role.cloudwatch_role.arn
  handler          = "cloudwatch_lambda.lambda_handler"
  source_code_hash = filebase64sha256("../bin/cloudwatch-lambda.zip")
  runtime          = "python3.8"
}

resource "aws_lambda_permission" "allow_cloudwatch" {
  statement_id  = "AllowCloudwatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.cloudwatch_lambda.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.rule.arn
}
