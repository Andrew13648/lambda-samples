data "aws_iam_policy_document" "sqs_policy" {

  statement {
    actions = [
      "sqs:ReceiveMessage",
      "sqs:DeleteMessage",
      "sqs:GetQueueAttributes"
    ]
    resources = [aws_sqs_queue.queue.arn]
  }

  statement {
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]
    resources = ["*"]
  }
}

resource "aws_iam_role" "sqs_role" {
  name               = "sqs-lambda-role"
  assume_role_policy = data.aws_iam_policy_document.lambda_assume_role.json
}

resource "aws_iam_role_policy" "sqs_role_policy" {
  name   = "sqs-role-policy"
  role   = aws_iam_role.sqs_role.name
  policy = data.aws_iam_policy_document.sqs_policy.json
}


