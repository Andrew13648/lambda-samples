resource "aws_dynamodb_table" "messages" {
  name             = "messages"
  billing_mode     = "PROVISIONED"
  read_capacity    = 1
  write_capacity   = 1
  hash_key         = "sender_id"
  range_key        = "message_id"
  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "sender_id"
    type = "S"
  }

  attribute {
    name = "message_id"
    type = "S"
  }
}
