resource "aws_lambda_function" "s3_lambda" {
  filename         = "../bin/s3-lambda.zip"
  function_name    = "s3-lambda"
  role             = aws_iam_role.s3_role.arn
  handler          = "s3_lambda.lambda_handler"
  source_code_hash = filebase64sha256("../bin/s3-lambda.zip")
  runtime          = "python3.8"
}

resource "aws_lambda_permission" "allow_bucket" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.s3_lambda.function_name
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.sample_bucket.arn
}
