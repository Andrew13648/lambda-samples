resource "aws_lambda_function" "dynamodb_lambda" {
  filename         = "../bin/dynamodb-lambda.zip"
  function_name    = "dynamodb-lambda"
  role             = aws_iam_role.dynamodb_role.arn
  handler          = "dynamodb_lambda.lambda_handler"
  source_code_hash = filebase64sha256("../bin/dynamodb-lambda.zip")
  runtime          = "python3.8"
}

resource "aws_lambda_event_source_mapping" "dynamodb_to_lambda" {
  event_source_arn                   = aws_dynamodb_table.messages.stream_arn
  function_name                      = aws_lambda_function.dynamodb_lambda.arn
  starting_position                  = "LATEST"
  maximum_batching_window_in_seconds = 20
}
