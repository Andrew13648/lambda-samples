data "aws_iam_policy_document" "cloudwatch_policy" {
  statement {
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]
    resources = ["*"]
  }
}

resource "aws_iam_role" "cloudwatch_role" {
  name               = "cloudwatch-lambda-role"
  assume_role_policy = data.aws_iam_policy_document.lambda_assume_role.json
}

resource "aws_iam_role_policy" "cloudwatch_role_policy" {
  name   = "cloudwatch-role-policy"
  role   = aws_iam_role.cloudwatch_role.name
  policy = data.aws_iam_policy_document.cloudwatch_policy.json
}
