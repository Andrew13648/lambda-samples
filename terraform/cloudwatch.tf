resource "aws_cloudwatch_event_rule" "rule" {
  name                = "sample-rule"
  schedule_expression = "rate(1 hour)"
}

resource "aws_cloudwatch_event_target" "target" {
  rule = aws_cloudwatch_event_rule.rule.name
  arn  = aws_lambda_function.cloudwatch_lambda.arn
}
