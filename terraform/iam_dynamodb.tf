data "aws_iam_policy_document" "dynamodb_policy" {
  statement {
    actions = [
      "dynamodb:DescribeStream",
      "dynamodb:GetRecords",
      "dynamodb:GetShardIterator"
    ]
    resources = [aws_dynamodb_table.messages.stream_arn]
  }
  statement {
    actions = [
      "dynamodb:ListStreams"
    ]
    resources = ["*"]
  }
  statement {
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]
    resources = ["*"]
  }
}

resource "aws_iam_role" "dynamodb_role" {
  name               = "dynamodb-lambda-role"
  assume_role_policy = data.aws_iam_policy_document.lambda_assume_role.json
}

resource "aws_iam_role_policy" "dynamodb_role_policy" {
  name   = "dynamodb-role-policy"
  role   = aws_iam_role.dynamodb_role.name
  policy = data.aws_iam_policy_document.dynamodb_policy.json
}

# Example of using a customer managed policy instead of an inline policy:
# resource "aws_iam_policy" "dynamodb_policy" {
#   name   = "dynamodb-lambda-policy"
#   policy = data.aws_iam_policy_document.dynamodb_policy.json
# }
# resource "aws_iam_role_policy_attachment" "attach_dynamodb_policy" {
#   role       = aws_iam_role.dynamodb_role.name
#   policy_arn = aws_iam_policy.dynamodb_policy.arn
# }

# Example of using AWS managed policy:
# resource "aws_iam_role_policy_attachment" "attach_dynamodb_policy" {
#   role       = aws_iam_role.dynamodb_role.name
#   policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaDynamoDBExecutionRole"
# }

